class Api::ApiController < ActionController::API
  include APITokenAuthenticatable

  respond_to :json
end
