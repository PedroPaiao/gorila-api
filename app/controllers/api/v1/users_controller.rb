class Api::V1::UsersController < Api::ApiController

  def create
    @user = User.create(
      email: create_user_params[:email],
      password: create_user_params[:password],
      password_confirmation: create_user_params[:password_confirmation]
    )

    render json: @user
  end

  private

  def create_user_params
    params.permit(:email, :password, :password_confirmation)
  end
end
